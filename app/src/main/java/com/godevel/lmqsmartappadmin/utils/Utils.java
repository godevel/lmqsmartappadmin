package com.godevel.lmqsmartappadmin.utils;

import android.content.Context;
import android.content.DialogInterface;

import androidx.appcompat.app.AlertDialog;

import com.godevel.lmqsmartappadmin.R;

import java.util.Base64;

public class Utils {

    public static boolean isTypeRevision(Integer appointmentType) {
        return appointmentType.intValue() == Constants.AT_R1.getId().intValue() || appointmentType.intValue() == Constants.AT_R2.getId().intValue()
                || appointmentType.intValue() == Constants.AT_R3.getId().intValue();
    }

    public static String getBasicAuthenticationKey() {
        String formattedUser = Constants.API_USER + ":" + Constants.API_KEY;
        return "Basic " + Base64.getEncoder().encodeToString(formattedUser.getBytes());
    }

    //ALERTS
    public static AlertDialog showAlert(Context context, int title, int message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context, getStyleId(null, context));
        builder.setTitle(context.getResources().getString(title))
                .setMessage(context.getResources().getString(message))
                .setCancelable(true)
                .setNegativeButton(R.string.btn_close, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // if this button is clicked, just close
                        // the dialog box and do nothing
                        dialog.cancel();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = builder.create();

        // show it
        alertDialog.show();
        return alertDialog;
    }

    public static AlertDialog showAlert(Context context, int title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context, getStyleId(null, context));
        builder.setTitle(context.getResources().getString(title))
                .setMessage(message)
                .setCancelable(true)
                .setNegativeButton(R.string.btn_close, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // if this button is clicked, just close
                        // the dialog box and do nothing
                        dialog.cancel();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = builder.create();

        // show it
        alertDialog.show();
        return alertDialog;
    }

    public static AlertDialog showAlert(Context context, String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context, getStyleId(null, context));
        builder.setTitle(title)
                .setMessage(message)
                .setCancelable(true)
                .setNegativeButton(R.string.btn_close, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // if this button is clicked, just close
                        // the dialog box and do nothing
                        dialog.cancel();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = builder.create();

        // show it
        alertDialog.show();
        return alertDialog;
    }

    public static AlertDialog showAlert(Context context, String title, int message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context, getStyleId(null, context));
        builder.setTitle(title)
                .setMessage(context.getResources().getString(message))
                .setCancelable(true)
                .setNegativeButton(R.string.btn_close, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // if this button is clicked, just close
                        // the dialog box and do nothing
                        dialog.cancel();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = builder.create();

        // show it
        alertDialog.show();
        return alertDialog;
    }

    public static int getStyleId(String title, Context context) {
        int id = 0;
        if (title == null) {
            id = context.getResources().getIdentifier("Theme_Custom_Dialog_Alert_No_Title", "style", context.getPackageName());
        } else {
            id = context.getResources().getIdentifier("Theme_Custom_Dialog_Alert", "style", context.getPackageName());
        }

        return id;
    }
}
