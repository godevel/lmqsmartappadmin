package com.godevel.lmqsmartappadmin;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NdefMessage;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.godevel.lmqsmartappadmin.gateway.AcuitySchedulingGateway;
import com.godevel.lmqsmartappadmin.gateway.UserInfoGateway;
import com.godevel.lmqsmartappadmin.models.Order;
import com.godevel.lmqsmartappadmin.models.User;
import com.godevel.lmqsmartappadmin.models.UserInfoModel;
import com.godevel.lmqsmartappadmin.models.UserSharedPref;
import com.godevel.lmqsmartappadmin.nfc.NFCManager;
import com.godevel.lmqsmartappadmin.utils.AES;
import com.godevel.lmqsmartappadmin.utils.Constants;
import com.godevel.lmqsmartappadmin.utils.Utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private UserInfoGateway userInfoGateway;

    private ListView list;
    private List<User> sortedUsers = new ArrayList<>();
    private List<User> filteredUsers = new ArrayList<>();

    private LinearLayout contentLayout;
    private ListViewAdapter adapter;
    private ProgressBar progressBar;
    private EditText txtSearch;

    private final Context context = this;
    private NFCManager nfcManager;
    private Tag tag;
    private PendingIntent pendingIntent;
    private IntentFilter writeTagFilters[];
    private boolean waitingRead = false;
    private boolean waitingWrite = false;
    private AlertDialog bringDialog;
    private User userToWrite;

    private List<Order> orders;

    private static String SAVED_INSTANCE = "SAVED_INSTANCE_LMQ_ADMIN";

    public void setSortedUsers(List<User> users) {
        sortedUsers = new ArrayList<>(users);
    }

    public List<User> getSortedUsers() {
        return sortedUsers;
    }

    public List<User> getFilteredUsers() {
        return filteredUsers;
    }

    public void setFilteredUsers(List<User> filteredUsers) {
        this.filteredUsers = new ArrayList<>(filteredUsers);
    }

    /* region CONSTRUCTOR */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //init objects
        setContentView(R.layout.activity_main);
        txtSearch = (EditText) findViewById(R.id.editText);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        list = (ListView) findViewById(R.id.list);
        contentLayout = (LinearLayout) findViewById(R.id.content_layout);

        //init data
        orders = new ArrayList<>();

        //init fc objects
        nfcManager = new NFCManager(this, context);
        pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
        IntentFilter tagDetected = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
        tagDetected.addCategory(Intent.CATEGORY_DEFAULT);
        writeTagFilters = new IntentFilter[] { tagDetected };

        // LISTENERS
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                onItemListClick(parent, view, position, id);
            }
        });

        txtSearch.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) { }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                onTextSearchChanged(s, start, before, count);
            }
        });

        //if screen rotate, then restore filtered values
        UserSharedPref userSharedPref = null;
        if (savedInstanceState != null) {
            userSharedPref = savedInstanceState.getParcelable(SAVED_INSTANCE);
        }

        if (savedInstanceState == null || userSharedPref == null) {
            //show progress bar
            setLoading(true);
            //load users and orders
            this.callUsers();
        } else {
            userSharedPref = savedInstanceState.getParcelable(SAVED_INSTANCE);
            if (userSharedPref != null) {
                List<User> sortUsers = userSharedPref.getSortedUsers();
                List<User> filtUsers = userSharedPref.getFilteredUsers();
                txtSearch.setText(userSharedPref.getUserNameFilter());

                orders.addAll(userSharedPref.getOrders());

                setFilteredUsers(filtUsers);
                setSortedUsers(sortUsers);
                adapter = new ListViewAdapter(MainActivity.this, filtUsers);
                list.setAdapter(adapter);

                setLoading(false);
            }
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        if (nfcManager == null) {
            nfcManager = new NFCManager(this, context);
        }

        if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(intent.getAction())) {
            tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
        }
        if (userToWrite != null) {

            if (waitingWrite) {
                NdefMessage tagMessage;
                tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
                waitingWrite = false;
                try {
                    String encryptedName = AES.encrypt(userToWrite.getFullNameForNFC());
                    tagMessage = nfcManager.createTextMessage(encryptedName);
                } catch (Exception e) {
                    Utils.showAlert(context, R.string.error_title, R.string.er_encrypt + "\n" + e.getMessage());
                    return;
                }

                if (tagMessage != null) {
                    try {
                        nfcManager.writeMessage(tagMessage, tag);
                        if (bringDialog != null) {
                            bringDialog.cancel();
                        }
                        Utils.showAlert(context, null, R.string.success_recording);
                    } catch (Exception ex) {
                        waitingWrite = false;
                        userToWrite = null;
                        if (bringDialog != null) {
                            bringDialog.cancel();
                        }
                        Utils.showAlert(context, R.string.error_title, R.string.er_recording);
                    }
                }
                userToWrite = null;
            }

            if (waitingRead) {
                waitingRead = false;
                try {
                    String encryptedName = readCard(intent);
                    if (bringDialog != null) {
                        bringDialog.cancel();
                    }
                    if (encryptedName == Constants.ER_NULL_POINTER_READ) {
                        return;
                    }
                    //IF CARD HAVE DATA, SHOW ALERT WITH DATA
                    if (!encryptedName.equals(Constants.EMPTY_TARGET) && (encryptedName.equals(Constants.DATA_FROM_OUTSIDE) || encryptedName != null)) {
                        overwriteCard(encryptedName, userToWrite);
                        return;
                    }

                    writeCard(userToWrite);

                } catch (Exception ex) {
                    waitingRead = false;
                    userToWrite = null;
                    Utils.showAlert(context, R.string.error_title, R.string.er_reading);
                    if (bringDialog != null) {
                        bringDialog.cancel();
                    }
                }

            }

        }

    }

    @Override
    public void onPause(){
        super.onPause();
        //stop nfc
        if (nfcManager == null) {
            nfcManager = new NFCManager(MainActivity.this, context);
        }
        nfcManager.WriteModeOff();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        UserSharedPref userSharedPref = new UserSharedPref();
        userSharedPref.setFilteredUsers(filteredUsers);
        userSharedPref.setSortedUsers(sortedUsers);
        userSharedPref.setUserNameFilter(txtSearch.getText().toString());
        userSharedPref.setOrders(orders);
        outState.putParcelable(SAVED_INSTANCE, userSharedPref);
    }

    @Override
    public void onResume(){
        super.onResume();

        //reset user info gateway
        userInfoGateway = new UserInfoGateway(this, bringDialog);
        userInfoGateway.setOrders(orders);
        String[][] techList = new String[][]{{android.nfc.tech.Ndef.class.getName()}, {android.nfc.tech.NdefFormatable.class.getName()}};

        try {
            if (nfcManager == null) {
                nfcManager = new NFCManager(MainActivity.this, context);
            }

            nfcManager.verifyNFC();
            nfcManager.WriteModeOn(pendingIntent, writeTagFilters, techList);
        } catch (NFCManager.NFCNotSupported nfcNotSupported) {
            Utils.showAlert(context, R.string.error_title, nfcNotSupported.getMessage());
        } catch (NFCManager.NFCNotEnabled nfcNotEnabled) {
            Utils.showAlert(context, R.string.error_title, nfcNotEnabled.getMessage());
        } catch (Exception ex) {
            Utils.showAlert(context, R.string.error_title, ex.getMessage());
        }
    }
    /* endregion CONSTRUCTOR */

    /* region EVENTS */
    private void onItemListClick(AdapterView<?> parent, View view, int position, long id) {
        //SHOW CONFIRM USER ALERT
        userToWrite = filteredUsers.get(position);
        String message = context.getResources().getString(R.string.record_configrmation) + " " + userToWrite.getFullName();
        AlertDialog.Builder builder = new AlertDialog.Builder(context, context.getResources().getIdentifier("Theme_Custom_Dialog_Alert_No_Title", "style", context.getPackageName()));
        builder.setMessage(message)
                .setCancelable(true)
                .setPositiveButton(R.string.btn_record, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        waitingRead = true;
                        dialog.cancel();
                        //SHOW BRING TARGET TO THE PHONE TO START RECORDING ALERT
                        bringDialog = Utils.showAlert(context, null, R.string.bring_target);
                        //READING....
                        bringDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialog) {
                                waitingRead = false;
                            }
                        });
                    }
                })
                .setNegativeButton(R.string.btn_read, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        onClickReadInformation(userToWrite);
                    }
                });
        // create alert dialog
        AlertDialog alertDialog = builder.create();
        // show it
        alertDialog.show();
    }

    private void onTextSearchChanged(CharSequence s, int start, int before, int count) {
        //Create new list with non filtered data and set to listview adapter
        List<User> allData = getSortedUsers();
        List<User> filtUsers = getFilteredUsers();
        int txtLength = txtSearch.getText().length();
        filtUsers.clear();
        for (int i = 0; i < allData.size(); i++) {
            if (txtLength <= allData.get(i).getFullName().length()) {
                if (allData.get(i).getFullName().toLowerCase().contains(txtSearch.getText().toString().toLowerCase().trim())) {
                    filtUsers.add(allData.get(i));
                }
            }
        }
        setFilteredUsers(filtUsers);
        adapter = new ListViewAdapter(MainActivity.this, filtUsers);
        list.setAdapter(adapter);
    }

    private void onClickReadInformation(User user) {
        if (bringDialog != null) {
            bringDialog.cancel();
        }

        bringDialog = Utils.showAlert(context, R.string.downloading, null);
        userInfoGateway.initDownload(user);
    }
    /* endregion EVENTS */

    /* region CALLS */
    /**
     * get all users
     */
    private void callUsers() {
        AcuitySchedulingGateway acuitySchedulingGateway = new AcuitySchedulingGateway();
        Call< List<User> > call = acuitySchedulingGateway.getAcuitySchedulingInterface().getAllUsers(Utils.getBasicAuthenticationKey());

        call.enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                if (response.isSuccessful()) {
                    List<User> users = response.body();
                    //sort list
                    try { //exclude exceptions
                        Collections.sort(users, User.nameComparator);
                    } catch (Exception e) {
                        Utils.showAlert(context, R.string.error_title, R.string.er_sorting);
                    }

                    //save list
                    setSortedUsers(users);
                    setFilteredUsers(users);
                } else if (response.code() == 401) {
                    Utils.showAlert(context, R.string.alert_title, R.string.er_token);
                    return;
                } else {
                    Utils.showAlert(context, R.string.alert_title, R.string.er_loading_users);
                    return;
                }

                callGetOrders();
            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {
                Utils.showAlert(context, R.string.alert_title, t.getMessage());
            }
        });
    }

    /**
     * get user orders
     */
    private void callGetOrders() {

        AcuitySchedulingGateway gateway = new AcuitySchedulingGateway();
        Call<List<Order>> call = gateway.getAcuitySchedulingInterface().getOrders(Utils.getBasicAuthenticationKey(), 2147483647);

        call.enqueue(new Callback<List<Order>>() {
            @Override
            public void onResponse(Call<List<Order>> call, Response<List<Order>> response) {
                if (userInfoGateway.errorAPIAction(response.code())) {
                    return;
                }

                userInfoGateway.setOrders(response.body());

                orders.addAll(response.body());

                adapter = new ListViewAdapter(context, getSortedUsers());
                list.setAdapter(adapter);
                setLoading(false);
            }

            @Override
            public void onFailure(Call<List<Order>> call, Throwable t) {
                userInfoGateway.onFailureAction();
            }
        });
    }
    /* endregion CALLS */

    /* region METHODS */
    /**
     * Show or hide spinner
     */
     private void setLoading(boolean loading) {
         if (loading) {
             contentLayout.setVisibility(View.INVISIBLE);
             progressBar.setVisibility(View.GONE);
             progressBar.setVisibility(View.VISIBLE);
         } else {
             progressBar.setVisibility(View.INVISIBLE);
             contentLayout.setVisibility(View.GONE);
             contentLayout.setVisibility(View.VISIBLE);
         }
    }

    /**
     * Action overwrite target with other user
     * @param encryptedName
     * @param newUser
     */
    private void overwriteCard(String encryptedName, User newUser) {
        User oldUser = this.searchByNameAndLastName(encryptedName);
        String msgDialog;
        if (oldUser == null) {
            msgDialog = context.getResources().getString(R.string.er_user_not_found_by_name_ovw);
        } else {
            msgDialog = context.getResources().getString(R.string.card_already_recorded)+" "+oldUser.getFullName()+"\n"+context.getResources().getString(R.string.want_overwrite);
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(context, context.getResources().getIdentifier("Theme_Custom_Dialog_Alert_No_Title", "style", context.getPackageName()));
        builder.setTitle(context.getResources().getString(R.string.alert_title))
                .setMessage(msgDialog)
                .setCancelable(true)
                .setNegativeButton(R.string.btn_close, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) { dialog.cancel(); }
                })
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        writeCard(newUser);
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = builder.create();
        // show it
        alertDialog.show();
    }

    /**
     * write card with user
     * @param user
     */
    private void writeCard(User user) {
        bringDialog = Utils.showAlert(context, null, R.string.recording);
        bringDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                waitingWrite = false;
            }
        });
        if (user.getFirstName() == null || user.getLastName() == null) {
            Utils.showAlert(context, R.string.alert_title, R.string.name_or_lastname_not_found);
            return;
        }

        waitingWrite = true;
    }

    /**
     * Read data from card
     * @param intent
     * @return
     */
    private String readCard(Intent intent) {
        try {
            return nfcManager.readFromIntent(intent);
        } catch (IOException e) {
            Utils.showAlert(context, R.string.error_title, R.string.er_reading);
            return Constants.ER_NULL_POINTER_READ;
        } catch (Exception e) { //have data but no from this system
            return Constants.DATA_FROM_OUTSIDE;
        }
    }

    private User searchByNameAndLastName (String fullName) {
        if (!fullName.contains(Constants.LINK_NFC)) {
            return null;
        }

        int index = fullName.indexOf(Constants.LINK_NFC);
        int endIndex = fullName.lastIndexOf(Constants.LINK_NFC) + Constants.LINK_NFC.length();
        String name = fullName.substring(0, index);
        String lastName = fullName.substring(endIndex);
        for(User user: getSortedUsers()) {
            if (user.getFirstName().equals(name) && user.getLastName().equals(lastName)) {
                return user;
            }
        }
        return null;
    }
    /* endregion METHODS */

    /* region public METHODS */
    public Context getContext() {
        return this.context;
    }

    public void startActivityUserInfo(UserInfoModel userInfoModel) {
        if (bringDialog != null) {
            bringDialog.cancel();
        }

        Intent intent = new Intent(MainActivity.this, UserInfoActivity.class);
        intent.putExtra(Constants.EXTRA_USER_INFO, userInfoModel);
        MainActivity.this.startActivity(intent);
    }
    /* endregion public METHODS */
}