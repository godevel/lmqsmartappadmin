package com.godevel.lmqsmartappadmin.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class UserSharedPref implements Parcelable {

    private List<Order> orders;

    private List<User> sortedUsers;

    private List<User> filteredUsers;

    private String userNameFilter;

    protected UserSharedPref(Parcel in) {
        userNameFilter = in.readString();
    }

    public UserSharedPref() {

    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    public List<User> getFilteredUsers() {
        return filteredUsers;
    }

    public void setFilteredUsers(List<User> filteredUsers) {
        this.filteredUsers = filteredUsers;
    }

    public List<User> getSortedUsers() {
        return sortedUsers;
    }

    public void setSortedUsers(List<User> sortedUsers) {
        this.sortedUsers = sortedUsers;
    }

    public String getUserNameFilter() {
        return userNameFilter;
    }

    public void setUserNameFilter(String userNameFilter) {
        this.userNameFilter = userNameFilter;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(userNameFilter);
    }

    public static final Creator<UserSharedPref> CREATOR = new Creator<UserSharedPref>() {
        @Override
        public UserSharedPref createFromParcel(Parcel in) {
            return new UserSharedPref(in);
        }

        @Override
        public UserSharedPref[] newArray(int size) {
            return new UserSharedPref[size];
        }
    };
}
