package com.godevel.lmqsmartappadmin;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ActionBar;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.godevel.lmqsmartappadmin.models.Appointment;
import com.godevel.lmqsmartappadmin.models.UserInfoModel;
import com.godevel.lmqsmartappadmin.utils.Constants;

import java.text.ParseException;
import java.util.List;

public class UserInfoActivity extends AppCompatActivity {
    private UserInfoModel userInfoModel;
    private final Context context = this;

    private TextView txtDay1;
    private TextView txtDay2;
    private TextView txtDay3;

    private TextView txtScheduledNum;
    private TextView txtNoScheduledNum;

    private TextView txtNextRevisionDate;
    private TextView txtFrequency;

    private TextView txtPayment;

    private TextView txtStartTreatmentDate;
    private TextView txtStartTreatmentDays;

    private TextView txtRevisionGoals;
    private TextView txtRevisionGoalsTitle;

    private TextView txtHistory;
    private TextView txtHistoryLast;

    private TextView txtName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_info);

        this.initView();
    }

    /* region METHODS */
    private void initView() {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN); //TO NO SHOW CONTROLS

        setContentView(R.layout.activity_user_info);

        //init views:
        txtDay1 = findViewById(R.id.txtDay1);
        txtDay2 = findViewById(R.id.txtDay2);
        txtDay3 = findViewById(R.id.txtDay3);

        txtScheduledNum = findViewById(R.id.txtScheduled);
        txtNoScheduledNum = findViewById(R.id.txtNoScheduledNum);

        txtNextRevisionDate = findViewById(R.id.txtNextRevisionDate);
        txtFrequency = findViewById(R.id.txtFrequency);

        txtPayment = findViewById(R.id.txtPayment);

        txtStartTreatmentDate = findViewById(R.id.txtStartTreatmentDate);
        txtStartTreatmentDays = findViewById(R.id.txtStartTreatmentDays);

        txtRevisionGoals = findViewById(R.id.txtRevisionGoals);
        txtRevisionGoalsTitle = findViewById(R.id.txtRevisionGoalsTitle);

        txtHistory = findViewById(R.id.txtHistory);
        txtHistoryLast = findViewById(R.id.txtHistoryLast);

        txtName = findViewById(R.id.txtName);

        this.loadData();
    }

    private void loadData() {
        userInfoModel = (UserInfoModel) getIntent().getSerializableExtra(Constants.EXTRA_USER_INFO);

        paintDays();
        paintNextVisits();
        paintNoScheduledVisits();
        boolean hasRevision = paintNextRevision();
        paintNextPayment(hasRevision);
        paintStartTreatmentDate();
        paintRevisionGoals();
        paintHistory();
        paintName();
    }

    private void paintDays() {
        if (userInfoModel == null) {
            txtDay1.setVisibility(View.INVISIBLE);
            txtDay2.setVisibility(View.INVISIBLE);
            txtDay3.setVisibility(View.INVISIBLE);

            return;
        }
        List<Appointment> nextTreeVisits = userInfoModel.getNextTreeVisits();

        if (nextTreeVisits == null || nextTreeVisits.isEmpty()) {
            txtDay1.setText(context.getResources().getString(R.string.txtNoAdjustments));
            txtDay2.setVisibility(View.INVISIBLE);
            txtDay3.setVisibility(View.INVISIBLE);
            return;
        }

        if (nextTreeVisits.size() > 0) {
            txtDay1.setText(nextTreeVisits.get(0).getDate() + " - " + nextTreeVisits.get(0).getTime());
        } else {
            txtDay1.setVisibility(View.INVISIBLE);
        }

        if (nextTreeVisits.size() > 1) {
            txtDay2.setText(nextTreeVisits.get(1).getDate() + " - " + nextTreeVisits.get(1).getTime());
        } else {
            txtDay2.setVisibility(View.INVISIBLE);
        }

        if (nextTreeVisits.size() > 2) {
            txtDay3.setText(nextTreeVisits.get(2).getDate() + " - " + nextTreeVisits.get(2).getTime());
        } else {
            txtDay3.setVisibility(View.INVISIBLE);
        }
    }

    private void paintNextVisits() {
        String msg = "";
        if (userInfoModel.getNextRevision() == null || userInfoModel.getNextRevision().equals("")) {
            msg = String.valueOf(context.getResources().getString(R.string.txtScheduledNoRevision));
        } else {
            msg = String.valueOf(context.getResources().getString(R.string.txtScheduled));
        }

        if (userInfoModel == null) {
            txtScheduledNum.setText("0 " + msg);
            return;
        }

        txtScheduledNum.setText(userInfoModel.getScheduledVisits() + " " + msg);
    }

    private void paintNoScheduledVisits() {
        if (userInfoModel == null) {
            txtNoScheduledNum.setText(String.valueOf("0 " + context.getResources().getString(R.string.txtNoScheduledNum)));
            return;
        }

        txtNoScheduledNum.setText(String.valueOf(userInfoModel.getNoScheduledVisits()) + " " + context.getResources().getString(R.string.txtNoScheduledNum));
    }

    private boolean paintNextRevision() {
        boolean hasNextRevision = false;
        if (userInfoModel == null) {
            return false;
        }

        if (userInfoModel.getNextRevision() == null || userInfoModel.getNextRevision().equals("")) {
            txtNextRevisionDate.setText(context.getResources().getString(R.string.txtNoScheduled));
        } else {
            txtNextRevisionDate.setText(userInfoModel.getNextRevision().getDate() + " - " + userInfoModel.getNextRevision().getTime());
            hasNextRevision = true;
        }

        if (userInfoModel.getFrequency() == null || userInfoModel.getFrequency().equals("")) {
            txtFrequency.setText("");
        } else {
            txtFrequency.setText(context.getResources().getString(R.string.txtCurrentFrequency).replace("{freq}", userInfoModel.getFrequency()));
        }

        return hasNextRevision;
    }

    private void paintNextPayment(boolean hasRevision) {
        if (userInfoModel == null || userInfoModel.getPaymentInfo() == null || userInfoModel.getPaymentInfo().equals("")) {
            txtPayment.setText(context.getResources().getString(R.string.noPaymentInfo));
            return;
        }

        txtPayment.setText(userInfoModel.getPaymentInfo());
    }

    private void paintStartTreatmentDate() {
        if (userInfoModel == null) {
            txtStartTreatmentDate.setText("");
            txtStartTreatmentDays.setText("");
            return;
        }

        txtStartTreatmentDate.setText(userInfoModel.getFirstAdjustment());
        try {
            long days = userInfoModel.getNumberDaysFromFirstAdjustment();
            if (days > 0) {
                txtStartTreatmentDays.setText(context.getResources().getString(R.string.startTreatmentDays).replace("{days}", String.valueOf(days)));
            } else {
                txtStartTreatmentDays.setText("");
            }
        } catch (ParseException e) {
            txtStartTreatmentDays.setText("");
        }
    }

    private void paintRevisionGoals() {
        if (userInfoModel == null || userInfoModel.getRevisionGoals() == null || userInfoModel.getRevisionGoals().equals("")) {
            txtRevisionGoalsTitle.setVisibility(View.INVISIBLE);
            txtRevisionGoals.setText("");
            return;
        }

        txtRevisionGoals.setText(userInfoModel.getRevisionGoals());
    }

    private void paintHistory() {
        if (userInfoModel == null) {
            txtHistory.setText("");
            txtHistoryLast.setText("");
            return;
        }

        if (userInfoModel.getHistoryAdjustments() > 0) {
            txtHistory.setText(userInfoModel.getHistoryAdjustments() + " " + context.getResources().getString(R.string.historyAdjustments));
        }

        try {
            long lastAdjustment = userInfoModel.getNumberDaysFromLastAdjustment();
            if (lastAdjustment == 0) {
                txtHistoryLast.setText(context.getResources().getString(R.string.lastAdjustmentToday));
            } else if (lastAdjustment > 0) {
                txtHistoryLast.setText(context.getResources().getString(R.string.lastAdjustment).replace("{days}", String.valueOf(lastAdjustment)));
            }
        } catch (ParseException e) {
            txtHistoryLast.setText("");
        }
    }

    private void paintName() {
        if (userInfoModel == null) {
            txtName.setText("");
            return;
        }
        String user = userInfoModel.getUser().getFirstName() + " " + userInfoModel.getUser().getLastName();
        txtName.setText(context.getResources().getString(R.string.lblUser).replace("{user}", user));
    }
}
