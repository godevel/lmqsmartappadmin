package com.godevel.lmqsmartappadmin.gateway;

import com.godevel.lmqsmartappadmin.utils.Constants;

import java.util.Base64;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AcuitySchedulingGateway {
    private Retrofit retrofit;
    private AcuitySchedulingInterface acuitySchedulingInterface;
    private String token;

    public AcuitySchedulingGateway() {
        retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        acuitySchedulingInterface = retrofit.create(AcuitySchedulingInterface.class);
    }

    public AcuitySchedulingInterface getAcuitySchedulingInterface() {
        return acuitySchedulingInterface;
    }
}
