package com.godevel.lmqsmartappadmin.gateway;

import androidx.appcompat.app.AlertDialog;

import com.godevel.lmqsmartappadmin.MainActivity;
import com.godevel.lmqsmartappadmin.R;
import com.godevel.lmqsmartappadmin.models.Appointment;
import com.godevel.lmqsmartappadmin.models.AppointmentType;
import com.godevel.lmqsmartappadmin.models.Certificate;
import com.godevel.lmqsmartappadmin.models.Field;
import com.godevel.lmqsmartappadmin.models.Form;
import com.godevel.lmqsmartappadmin.models.Order;
import com.godevel.lmqsmartappadmin.models.User;
import com.godevel.lmqsmartappadmin.models.UserInfoModel;
import com.godevel.lmqsmartappadmin.utils.Constants;
import com.godevel.lmqsmartappadmin.utils.Utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserInfoGateway {

    private final int FROM_GET_OLD = 0;
    private final int FROM_GET_NEXT = 1;

    private MainActivity mainActivity;
    private AlertDialog bringDialog;
    private int pendingLoops;
    private UserInfoModel userInfoModel;
    private List<Order> orders;

    public UserInfoGateway(MainActivity mainActivity, AlertDialog bringDialog) {
        this.mainActivity = mainActivity;
        this.bringDialog = bringDialog;
        this.pendingLoops = 0;
        this.userInfoModel = new UserInfoModel();
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
        userInfoModel.setOrders(orders);
    }

    public void initDownload(User user) {
        this.userInfoModel.setUser(user);
        this.callGetPaymentInfo();
    }

    /* region CALLS */
    /**
     * download payment info
     */
    private void callGetPaymentInfo() {
        User auxUser = userInfoModel.getUser();
        AcuitySchedulingGateway gateway = new AcuitySchedulingGateway();
        Call<List<Appointment>> call = gateway.getAcuitySchedulingInterface().getAppointmentByType(Utils.getBasicAuthenticationKey(), auxUser.getFirstName(), auxUser.getLastName(), Constants.FIRST_VISIT.getId());

        call.enqueue(new Callback<List<Appointment>>() {
            @Override
            public void onResponse(Call<List<Appointment>> call, Response<List<Appointment>> response) {
                if (errorAPIAction(response.code())) {
                    return;
                }

                List<Appointment> appointments = response.body();

                if (appointments != null && !appointments.isEmpty()) {
                    List<Form> forms = appointments.get(0).getForms();
                    for(Form form : forms) {
                        if (form.getId() == Constants.ID_BASIC_FORM){
                            for(Field field : form.getValues()) {
                                if (field.getFieldID() == Constants.FIELD_ID_FIELD_NEXT_PAYMENT) {
                                    userInfoModel.setPaymentInfo(field.getValue());
                                }
                            }
                        }
                    }
                }

                callGetOldAppointments();
            }

            @Override
            public void onFailure(Call<List<Appointment>> call, Throwable t) {
                onFailureAction();
            }
        });
    }

    /**
     * download old appointments
     */
    private void callGetOldAppointments() {
        User auxUser = userInfoModel.getUser();

        AcuitySchedulingGateway gateway = new AcuitySchedulingGateway();
        Call<List<Appointment>> call = gateway.getAcuitySchedulingInterface().getOldVisits(Utils.getBasicAuthenticationKey(), auxUser.getFirstName(), auxUser.getLastName(), getToday(), "ASC");

        call.enqueue(new Callback<List<Appointment>>() {

            @Override
            public void onResponse(Call<List<Appointment>> call, Response<List<Appointment>> response) {
                if (errorAPIAction(response.code())) {
                    return;
                }

                List<Appointment> auxAppointments = response.body();
                List<Appointment> oldAppointments = new ArrayList<>();
                int indexRevision = -1;

                for (int i = 0; i < auxAppointments.size(); i++) {
                    Appointment aux = auxAppointments.get(i);
                    if(aux.getAppointmentTypeID().intValue() == Constants.ADJUSTMENT.getId().intValue()){
                        oldAppointments.add(auxAppointments.get(i));
                    }

                    if (aux.getAppointmentTypeID().intValue() == Constants.INITIAL_INFO.getId().intValue() || Utils.isTypeRevision(aux.getAppointmentTypeID())) {
                        indexRevision = i;
                    }
                }

                //save form goals and frequency
                if (indexRevision != -1) {
                    saveGoals(auxAppointments, indexRevision);
                }

                //save info for next activity
                userInfoModel.setOldAppointments(oldAppointments);

                callGetNextAppointments();
            }

            @Override
            public void onFailure(Call<List<Appointment>> call, Throwable t) {
                onFailureAction();
            }
        });
    }

    /**
     * download next appointments
     */
    private void callGetNextAppointments() {
        User user = userInfoModel.getUser();
        AcuitySchedulingGateway gateway = new AcuitySchedulingGateway();
        Call<List<Appointment>> call = gateway.getAcuitySchedulingInterface().getNextVisits(Utils.getBasicAuthenticationKey(), user.getFirstName(), user.getLastName(), getToday(), "ASC");

        call.enqueue(new Callback<List<Appointment>>() {

            @Override
            public void onResponse(Call<List<Appointment>> call, Response<List<Appointment>> response) {
                if (errorAPIAction(response.code())) {
                    return;
                }

                List<Appointment> auxAppointments = response.body();
                List<Appointment> nextTreeAppointments = new ArrayList<>();
                int indexRevision = -1;

                List<Appointment> nextAppointmentsToNextRevision = new ArrayList<>();

                //get next tree visits
                if (!auxAppointments.isEmpty()) {
                    for (int i = 0; i < Constants.NUMBER_NEXT_VISITS; i++) {
                        if (i < auxAppointments.size()) {
                            Appointment aux = auxAppointments.get(i);
                            if (aux.getAppointmentTypeID().intValue() == Constants.ADJUSTMENT.getId().intValue()) {
                                nextTreeAppointments.add(auxAppointments.get(i));
                            }
                        }
                    }
                }

                //get next adjustment appointments to the next visit and next revision
                for (int i = 0; i < auxAppointments.size(); i++) {
                    Appointment aux = auxAppointments.get(i);

                    if (aux.getAppointmentTypeID().intValue() == Constants.ADJUSTMENT.getId().intValue()) {
                        nextAppointmentsToNextRevision.add((auxAppointments.get(i)));
                    }
                    if (Utils.isTypeRevision(aux.getAppointmentTypeID())){
                        AppointmentType nextRevision = new AppointmentType(
                                aux.getId(), aux.getType(), aux.getDate(),
                                aux.getTime(), aux.getEndTime(), aux.getDatetime());

                        userInfoModel.setNextRevision(nextRevision);

                        break;
                    }
                    if (aux.getAppointmentTypeID().intValue() == Constants.INITIAL_INFO.getId().intValue()) {
                        indexRevision = i;
                    }
                }

                //save form goals and frequency
                if (indexRevision != -1 && userInfoModel.getRevisionGoals() == null) {
                    saveGoals(auxAppointments, indexRevision);
                }

                //save info for next activity
                userInfoModel.setNextTreeVisits(nextTreeAppointments);
                userInfoModel.setNextAppointmentsToNextRevision(nextAppointmentsToNextRevision);

                List<Order> auxOrders = new ArrayList<>();
                String auxName = user.getFirstName().toUpperCase();
                String auxLastName = user.getLastName().toUpperCase();

                if (orders != null && !orders.isEmpty()) {
                    for (Order order : orders) {
                        if (order.getFirstName().toUpperCase().equals(auxName) && order.getLastName().toUpperCase().equals(auxLastName)) {
                            auxOrders.add(order);
                        }
                    }
                }

                userInfoModel.setOrders(auxOrders);

                if (auxOrders != null && auxOrders.size() > 0) {
                    pendingLoops = auxOrders.size();
                    for (Order order : auxOrders) {
                        getCertificates(order.getId());
                    }
                } else {
                    mainActivity.startActivityUserInfo(userInfoModel);
                }
            }

            @Override
            public void onFailure(Call<List<Appointment>> call, Throwable t) {
                onFailureAction();
            }
        });
    }

    /**
     * Get certificate
     * @param orderId
     */
    private void getCertificates(int orderId) {
        AcuitySchedulingGateway gateway = new AcuitySchedulingGateway();
        Call<List<Certificate>> call = gateway.getAcuitySchedulingInterface().getCertificates(Utils.getBasicAuthenticationKey(), orderId);

        call.enqueue(new Callback<List<Certificate>>() {

            @Override
            public void onResponse(Call<List<Certificate>> call, Response<List<Certificate>> response) {
                if (response.code() == 200) {
                    //save certificate
                    for (Certificate certificate : response.body())
                    {
                        userInfoModel.addCertificate(certificate);
                    }
                }

                //exit when finish
                pendingLoops--;
                if (pendingLoops == 0) {
                    mainActivity.startActivityUserInfo(userInfoModel);
                }
            }

            @Override
            public void onFailure(Call<List<Certificate>> call, Throwable t) {
                onFailureAction();
            }
        });
    }

    /**
     * get certificate
     * @param certificate
     * @param appointmentTypeID
     * @param origin
     */
    private void callCheckCertificate(String certificate, Integer appointmentTypeID, int origin) {
        final int auxOrigin = origin;
        User auxUser = userInfoModel.getUser();
        AcuitySchedulingGateway gateway = new AcuitySchedulingGateway();
        Call<Certificate> call = gateway.getAcuitySchedulingInterface().checkCertificate(Utils.getBasicAuthenticationKey(), certificate, appointmentTypeID);

        call.enqueue(new Callback<Certificate>() {

            @Override
            public void onResponse(Call<Certificate> call, Response<Certificate> response) {
                if (response.code() == 200) {
                    //save certificate
                    userInfoModel.addCertificate(response.body());
                }

                switch (auxOrigin){
                    case FROM_GET_OLD:
                        //exit when finish
                        pendingLoops--;
                        if (pendingLoops == 0) {
                            callGetNextAppointments();
                        }
                        break;
                    case FROM_GET_NEXT:
                        //exit when finish
                        pendingLoops--;
                        if (pendingLoops == 0) {
                            mainActivity.startActivityUserInfo(userInfoModel);
                        }
                        break;
                }
            }

            @Override
            public void onFailure(Call<Certificate> call, Throwable t) {
                pendingLoops = 0;
                if (bringDialog != null) {
                    bringDialog.cancel();
                }
                bringDialog = Utils.showAlert(mainActivity.getContext(), R.string.error_title, R.string.er_rf);
            }
        });
    }

    /* endregion CALLS */

    /* region private METHODS */
    private Date getToday() {
        return new Date();
    }

    private void saveGoals(List<Appointment> appointments, int indexRevision) {
        for (Form form : appointments.get(indexRevision).getForms()) {
            if (form.getId() == Constants.ID_FIRS_REV_PLAN_FORM) {
                for (Field field : form.getValues()) {
                    if (field.getFieldID() == Constants.FIELD_ID_FIRS_REV_NEXT_META) {
                        userInfoModel.setRevisionGoals(field.getValue());
                    }
                    if (field.getFieldID() == Constants.FIELD_ID_FIRS_REV_FREQUENCY) {
                        userInfoModel.setFrequency(field.getValue());
                    }
                }
            } else if (form.getId() == Constants.ID_REVISION_PLAN_FORM) {
                for (Field field : form.getValues()) {
                    if (field.getFieldID() == Constants.FIELD_ID_NEXT_META) {
                        userInfoModel.setRevisionGoals(field.getValue());
                    }
                    if (field.getFieldID() == Constants.FIELD_ID_FREQUENCY) {
                        userInfoModel.setFrequency(field.getValue());
                    }
                }
            }
        }
    }

    public boolean errorAPIAction(int code) {
        if (code != 200) {
            if (bringDialog != null) {
                bringDialog.cancel();
            }
            bringDialog = Utils.showAlert(mainActivity.getContext(), R.string.error_title, R.string.er_rf);

            return true;
        }
        return false;
    }

    public void onFailureAction () {
        pendingLoops = 0;
        if (bringDialog != null) {
            bringDialog.cancel();
        }
        bringDialog = Utils.showAlert(mainActivity.getContext(), R.string.error_title, R.string.er_rf);
    }
    /* endregion private METHODS */
}
